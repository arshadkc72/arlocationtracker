//
//  ADDLocationVC.swift
//  ARKitCompassRose
//
//  Created by Sharanabasappa-Macmini on 12/09/17.
//  Copyright © 2017 vasile.ch. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
class ADDLocationVC: UIViewController,LocationManagerDelegate{

    @IBOutlet weak var mapVIew: MKMapView?
    override func viewDidLoad() {
        super.viewDidLoad()
        mapVIew?.delegate = self
        mapVIew?.showsUserLocation = true
        self.navigationController?.navigationBar.isHidden = true
//        LocationManager.sharedInstance.delegate = self
//
//        LocationManager.sharedInstance.locationManager.requestWhenInUseAuthorization()
        //LocationManager.sharedInstance.startUpdating()
        addAnotation((LocationManager.sharedInstance.currentLocation?.coordinate)!)
        // Do any additional setup after loading the view.
    }
    func addAnotation(_ location : CLLocationCoordinate2D){
   
        let region = MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        self.mapVIew?.setRegion(region, animated: true)
        
        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = location
        pointAnnotation.title = ""
        mapVIew?.addAnnotation(pointAnnotation)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    @IBAction func addAnnotation(_ sender: Any) {
        let defaults = UserDefaults.standard
      
        let placesData2 = defaults.object(forKey: "Places") as? NSData
        
        if let placesData2 = placesData2 {
            let placesArray = NSKeyedUnarchiver.unarchiveObject(with: placesData2 as Data) as? [CLLocation]
            
            if let placesArray = placesArray {
                var annotaionArray = placesArray
                annotaionArray.append(CLLocation(latitude: (mapVIew?.centerCoordinate.latitude)!, longitude: (mapVIew?.centerCoordinate.longitude)!))
                
                let placesData1 = NSKeyedArchiver.archivedData(withRootObject: annotaionArray)
                defaults.set(placesData1, forKey: "Places")
                UserDefaults.standard.synchronize()
            }
            else{
                let placesData1 = NSKeyedArchiver.archivedData(withRootObject: [CLLocation(latitude: (mapVIew?.centerCoordinate.latitude)!, longitude: (mapVIew?.centerCoordinate.longitude)!)])
                defaults.set(placesData1, forKey: "Places")
                
                UserDefaults.standard.synchronize()
            }
            
        }
        else{
            let placesData1 = NSKeyedArchiver.archivedData(withRootObject: [CLLocation(latitude: (mapVIew?.centerCoordinate.latitude)!, longitude: (mapVIew?.centerCoordinate.longitude)!)])
            defaults.set(placesData1, forKey: "Places")
            
            UserDefaults.standard.synchronize()
        }
 
        addAnotation((mapVIew?.centerCoordinate)!)
        mapVIew?.add(MKCircle(center: (mapVIew?.centerCoordinate)!, radius: 50))
 
    }
    
    @IBAction func DoneAction(_ sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
}
extension ADDLocationVC : MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        else {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "annotationView") ?? MKAnnotationView()
            annotationView.image = UIImage(named: "home2.png")
            return annotationView
        }
       
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKCircleRenderer(overlay: overlay)
        renderer.fillColor = UIColor.black.withAlphaComponent(0.3)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 2
        return renderer
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        // let region = MKCoordinateRegion(center: (self.mapView?.userLocation.coordinate)!, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
        //mapView.setRegion(region, animated: true)
    }
}
