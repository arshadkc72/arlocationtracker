//
//  CardinalDirection.swift
//  ARKitCompassRose
//
//  Created by Vasile Cotovanu on 25.07.17.
//  Copyright © 2017 vasile.ch. All rights reserved.
//

import UIKit
import SceneKit
import CoreLocation
import simd
import GLKit

let nameList : [String] = ["A","B","c","D","E"]
let loc = CLLocation(latitude:12.957122,longitude:  77.649118)
//LocationsList.append(loc)
let loc2 = CLLocation(latitude:12.958889, longitude:   77.645417)
//LocationsList.append(loc2)
let loc3 = CLLocation(latitude: 12.961973, longitude:  77.635407 )
//LocationsList.append(loc3)
let loc4 = CLLocation(latitude: 12.960582,  longitude:  77.645079  )
//LocationsList.append(loc4)
let loc5 = CLLocation(latitude: 12.961029,   longitude:  77.647117 )
var LocationsList :[CLLocation] = [loc,loc2,loc3,loc4,loc5]
class CardinalDirection: NSObject {
    var name: String
    var angle: Float
    
    init(name: String, angle: Float) {
        self.name = name
        self.angle = angle
        
        super.init()
    }
    
    func sphere(currentLocation: CLLocationCoordinate2D,endloc : CLLocationCoordinate2D,locname:String) -> SCNNode {
        let sphere = SCNSphere(radius: 5)
        
        let sphereMaterial = SCNMaterial()
        
        sphereMaterial.diffuse.contents = UIColor.red
        
        let str =  String(Double(round(100000*(BearAngle.distanceTwopoints(map1: currentLocation, map2: endloc)/1000))/100000) )
        
        
        let textBlock = SCNText(string: "\(str)", extrusionDepth: 4)
        textBlock.font.withSize(5.0)
        textBlock.flatness = -2.0
        textBlock.firstMaterial?.diffuse.contents = UIColor.blue
        textBlock.alignmentMode = kCAAlignmentCenter
        textBlock.font = UIFont(name: "Helvatica", size: 1.0)
        
        let node1 = SCNNode(geometry: textBlock)
        
        //node1.name = locname //String(endloc.coordinate.latitude)+String(endloc.coordinate.longitude)
        node1.scale = SCNVector3Make( 0.5, 0.5, 0.5);
        let paresntnode = SCNNode(geometry: sphere)
        paresntnode.name = locname
        node1.position = SCNVector3(-5, -7,10)
        paresntnode.geometry?.materials = [sphereMaterial]
   
        paresntnode.transform = CardinalDirection.getTransformGiven(currentLocation: currentLocation, endLocation: endloc)
             paresntnode.addChildNode(node1)
        //transform(rotationY: GLKMathDegreesToRadians(angle), distance: 100)
        
        return paresntnode
    }
    
    func text() -> SCNNode {
        let textBlock = SCNText(string: "\(name) \(angle)°", extrusionDepth: 0.5)
        textBlock.firstMaterial?.diffuse.contents = UIColor.blue
        
        let node = SCNNode(geometry: textBlock)
        node.transform = CardinalDirection.transform(rotationY: GLKMathDegreesToRadians(angle), distance: 100)
        
        return node
    }
    
    static func transform(rotationY: Float, distance: Int) -> SCNMatrix4 {
        
        // Translate first on -z direction
        let translation = SCNMatrix4MakeTranslation(0, 0, Float(-distance))
        // Rotate (yaw) around y axis
        let rotation = SCNMatrix4MakeRotation(-1 * rotationY, 0, 1, 0)
        
        // Final transformation: TxR
        let transform = SCNMatrix4Mult(translation, rotation)
        
        return transform
    }
    class func getTransformGiven(currentLocation: CLLocationCoordinate2D,endLocation : CLLocationCoordinate2D) -> SCNMatrix4 {
        
        let bearing = BearAngle.bearingBetween(
            startLocation: currentLocation,
            endLocation: endLocation
        )
        let distance = 100  //BearAngle.distanceTwopoints(map1: currentLocation, map2: endLocation)
  
        let translation = SCNMatrix4MakeTranslation(0, 0, Float(-distance))
        // Rotate (yaw) around y axis
        let rotation = SCNMatrix4MakeRotation(-1 * GLKMathDegreesToRadians(bearing), 0, 1, 0)
  
        let transform = SCNMatrix4Mult(translation, rotation)
    
        return transform
    }
}

extension CardinalDirection {
//CardinalDirection(name: "N", angle: 0),
//    CardinalDirection(name: "NE", angle: 45),
//    CardinalDirection(name: "E", angle: 90),
//    CardinalDirection(name: "SE", angle: 135),
//    CardinalDirection(name: "S", angle: 180),
    //CardinalDirection(name: "SW", angle: 225),
    // CardinalDirection(name: "W", angle: 270),
    //CardinalDirection(name: "NW", angle: 315),
//    static let defaults: [CardinalDirection] = {
//        return[
//            CardinalDirection(name: "N", angle: BearAngle.bearingBetween(
//                startLocation:LocationManager.sharedInstance.currentLocation.cordinate,
//                endLocation:LocationsList[0]
//            )),
//            CardinalDirection(name: "NE", angle:  BearAngle.bearingBetween(
//                startLocation:LocationManager.sharedInstance.currentLocation!,
//                endLocation:LocationsList[1]
//            )),
//            CardinalDirection(name: "E", angle:  BearAngle.bearingBetween(
//                startLocation:LocationManager.sharedInstance.currentLocation!,
//                endLocation:LocationsList[2]
//            )),
//            CardinalDirection(name: "SE", angle:  BearAngle.bearingBetween(
//                startLocation:LocationManager.sharedInstance.currentLocation!,
//                endLocation:LocationsList[3]
//            )),
//            CardinalDirection(name: "W", angle:  BearAngle.bearingBetween(
//                startLocation:LocationManager.sharedInstance.currentLocation!,
//                endLocation:LocationsList[4]
//            )),
//
            //CardinalDirection(name: "SW", angle: 225),
           // CardinalDirection(name: "W", angle: 270),
            //CardinalDirection(name: "NW", angle: 315),
//        ]
//    }()
}




