//
//  LocationMange.swift
//  ARKitCompassRose
//
//  Created by Sharanabasappa-Macmini on 05/09/17.
//  Copyright © 2017 vasile.ch. All rights reserved.
//

import Foundation
import CoreLocation
import GLKit
import SceneKit
public class BearAngle  {
class func bearingBetween(startLocation: CLLocationCoordinate2D, endLocation: CLLocationCoordinate2D) -> Float {
    var azimuth: Float = 0
    let lat1 = GLKMathDegreesToRadians(
        Float(startLocation.latitude)
    )
    let lon1 = GLKMathDegreesToRadians(
        Float(startLocation.longitude)
    )
    let lat2 = GLKMathDegreesToRadians(
        Float(endLocation.latitude)
    )
    let lon2 = GLKMathDegreesToRadians(
        Float(endLocation.longitude)
    )
    let dLon = lon2 - lon1
    let y = sin(dLon) * cos(lat2)
    let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
    let radiansBearing = atan2(y, x)
    azimuth = GLKMathRadiansToDegrees(Float(radiansBearing))
    if(azimuth < 0) {
        azimuth += 360
        
    }
    return azimuth
}
 
   class func distanceTwopoints(map1:CLLocationCoordinate2D,map2:CLLocationCoordinate2D)-> Double{
        let coordinate₀ = CLLocation(latitude: map1.latitude, longitude: map1.longitude)
   
        let coordinate₁ = CLLocation(latitude: map2.latitude, longitude: map2.longitude)
        
        return coordinate₀.distance(from: coordinate₁) 
    }
}
