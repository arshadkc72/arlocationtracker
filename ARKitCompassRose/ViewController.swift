//
//  ViewController.swift
//  ARKitCompassRose
//
//  Created by Vasile Cotovanu on 25.07.17.
//  Copyright © 2017 vasile.ch. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import CoreLocation
struct PreferencesKeys {
    static let savedItems = "savedRegions"
}
class ViewController: UIViewController, ARSCNViewDelegate,LocationManagerDelegate {
    var LocationsList :[CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
    var isaddeed = true
    @IBOutlet var sceneView: ARSCNView!
    public var sceneNode: SCNNode?
    var smallDistanceAngle : Float?
    var neaestDestination : Double?
    override func viewDidLoad() {
        super.viewDidLoad()
        LocationManager.sharedInstance.delegate = self
        LocationManager.sharedInstance.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        LocationManager.sharedInstance.locationManager.distanceFilter = 2
        LocationManager.sharedInstance.locationManager.requestWhenInUseAuthorization()
        self.navigationController?.navigationBar.isTranslucent = false
        sceneView.delegate = self
        sceneView.showsStatistics = true
        let scene = SCNScene()
        //        sceneNode = SCNNode()
        //        sceneView.scene.rootNode.addChildNode(sceneNode!)
        
        sceneView.scene = scene
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        let defaults = UserDefaults.standard
        let placesData2 = defaults.object(forKey: "Places") as? NSData
        LocationsList.removeAll()
        smallDistanceAngle = nil
        neaestDestination = nil
        let configuration = ARWorldTrackingSessionConfiguration()
        // Align the real world on z(North-South) x(West-East) axis
        
        configuration.planeDetection = .horizontal
        configuration.worldAlignment = .gravityAndHeading
        if let placesData2 = placesData2 {
            let placesArray = NSKeyedUnarchiver.unarchiveObject(with: placesData2 as Data) as? [CLLocation]
            if let placesArray = placesArray {
                for place in placesArray {
                    LocationsList.append(place.coordinate)
                }
                
                isaddeed = true
            }
            //            updateLocation()
            //            isaddeed = false
        }
        LocationManager.sharedInstance.startUpdating()
        
        
        //
        
        sceneView.session.run(configuration, options: .resetTracking)
        
        
    }
    
    func didSetLocation(location: CLLocation?){
        if isaddeed {
            isaddeed = false
            updateLocation()
        }
        else{
            guard let curLoc  = LocationManager.sharedInstance.currentLocation else  {
                return
            }
            
            
            var index = 0
            for locs in LocationsList {
                let sphereNode = sceneNode?.childNode(withName: String(index), recursively: true)
                guard let childnodes = sphereNode?.childNodes else {
                    return
                }
                for textnode in childnodes {
                    guard let node2 = textnode.geometry as? SCNText else{
                        return
                    }
                    if smallDistanceAngle == nil {
                        neaestDestination = BearAngle.distanceTwopoints(map1:  curLoc.coordinate, map2: LocationsList[index])
                        smallDistanceAngle =  BearAngle.bearingBetween(startLocation: curLoc.coordinate, endLocation: LocationsList[index])
                    }
                    else
                    {  let newDestination = BearAngle.distanceTwopoints(map1:  curLoc.coordinate, map2: LocationsList[index])
                        let newAngle =  BearAngle.bearingBetween(startLocation: curLoc.coordinate, endLocation: LocationsList[index])
                        if newDestination < neaestDestination! {
                            neaestDestination = newDestination
                            smallDistanceAngle = newAngle
                        }
                        
                    }
                    
                    SCNTransaction.begin()
                    //node2.string  =
                    node2.string =  String(Double(round(100000*(BearAngle.distanceTwopoints(map1: curLoc.coordinate, map2: LocationsList[index])/1000))/100000) )
                    index = index + 1
                    SCNTransaction.commit()
                }
                
                
            }
        }
        
    }
    func updateLocation(){
        let constraint = SCNLookAtConstraint(target: sceneView.pointOfView)
        
        // Keep the rotation on the horizon
        constraint.isGimbalLockEnabled = true
        
        // Slow the constraint down a bit
        constraint.influenceFactor = 0.01
        
        // Finally add the constraint to the node
        
        let scene = SCNScene()
        scene.rootNode.constraints = [constraint]
        var index = 0
        for direction in LocationsList {
            guard let loc = LocationManager.sharedInstance.currentLocation else {
                
                return
            }
            if smallDistanceAngle == nil {
                neaestDestination = BearAngle.distanceTwopoints(map1:  (LocationManager.sharedInstance.currentLocation?.coordinate)!, map2: LocationsList[index])
                smallDistanceAngle =  BearAngle.bearingBetween(startLocation: (LocationManager.sharedInstance.currentLocation?.coordinate)!, endLocation: LocationsList[index])
            }
            else
            {  let newDestination = BearAngle.distanceTwopoints(map1:  (LocationManager.sharedInstance.currentLocation?.coordinate)!, map2: LocationsList[index])
                let newAngle =   BearAngle.bearingBetween(startLocation: (LocationManager.sharedInstance.currentLocation?.coordinate)!, endLocation: LocationsList[index])
                if newDestination < neaestDestination! {
                    neaestDestination = newDestination
                    smallDistanceAngle = newAngle
                }
                
            }
            let direction = CardinalDirection(name: "a", angle: 0.0)
            scene.rootNode.addChildNode(direction.sphere(currentLocation: loc.coordinate, endloc: LocationsList[index],locname:String(index)))
            
            //scene.rootNode.addChildNode(direction.text())
            // sceneView.session.add(anchor: )
            //scene.rootNode.addChildNode(direction.text())
            index = index + 1;
        }
        sceneNode = scene.rootNode
        // sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        sceneView.scene = scene
        //sceneView.projectPoint()
    }
    func getCurrentcursorpos()-> SCNVector3?{
        guard let currPos = sceneView.pointOfView else {
            return nil
        }
        return sceneView.scene.rootNode.convertPosition(currPos.worldPosition, to: sceneNode)
    }
    func didUpdateHeading(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading){
        if newHeading.headingAccuracy < 0 {
            debugPrint(newHeading.trueHeading)
            addDirectionPlane(direction:newHeading.trueHeading)
        }
        else {
            addDirectionPlane(direction:newHeading.magneticHeading)
            debugPrint(newHeading.magneticHeading)
        }
        
        
    }
    
    func addDirectionPlane(direction:CLLocationDirection){
        let arrowPlane = sceneView.pointOfView?.childNode(withName: "arrow", recursively: false)
        guard let angle = smallDistanceAngle else {
            return
        }
        guard let planes = arrowPlane?.geometry as? SCNBox else {
            let plane = SCNBox(width: 10.0, height: 40.0, length: 5.0, chamferRadius: 0.0)
            plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "mystica-Arrow-set-Bulb-2-300px")
            //plane.firstMaterial?.diffuse.contents = UIColor.red
            //            let upperPoint =  Int(smallDistanceAngle!) + 20
            //            let lowerPoint = Int(smallDistanceAngle!) - 20
            //
            //            let range = lowerPoint...upperPoint
            //            if (lowerPoint / 90) == 4 && (upperPoint/90) == 1 {
            //                if Int(direction) < lowerPoint {
            //                   plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
            //                }
            //                else   if Int(direction) > upperPoint {
            //                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
            //                }
            //                else{
            //                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "mystica-Arrow-set-Bulb-2-300px")
            //                }
            //            }
            //            else {
            //            if range.contains(Int(direction)){
            //                plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "mystica-Arrow-set-Bulb-2-300px")
            //            }
            //            else if Int(direction) < lowerPoint {
            //                plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
            //            }
            //            else if Int(direction) > upperPoint {
            //                plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
            //            }
            //            }
            let smallDistanceQuardant  = Int(ceil(smallDistanceAngle! / 90.0))
            let currentQuartdant  = Int(ceil(direction / 90.0))
            
            var lowerRange =  (Int(smallDistanceAngle! - 5))...Int(smallDistanceAngle!)
            var upperRange = (Int(smallDistanceAngle! + 1))...(Int(smallDistanceAngle! + 5))
            if (Int(smallDistanceAngle! - 5)) <= 360 && (Int(smallDistanceAngle! + 5)) >= 360 {
                // centerVal = 360
                lowerRange =  (Int(smallDistanceAngle! - 5))...360
                upperRange = (0)...(Int(smallDistanceAngle! + 5))
            }
            
            
            
            if lowerRange.contains(Int(direction)) || upperRange.contains(Int(direction)) {
                plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "mystica-Arrow-set-Bulb-2-300px")
            }
            else if currentQuartdant == 4 {
                if smallDistanceQuardant == 1 || smallDistanceQuardant == 2 {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
                }
                else if smallDistanceQuardant == 3 {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
                }
            }
            else if currentQuartdant == 3 {
                if smallDistanceQuardant == 4 || smallDistanceQuardant == 1{
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
                }
                else if smallDistanceQuardant == 2 {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
                }
            }
            else if currentQuartdant == 2{
                if smallDistanceQuardant == 1 || smallDistanceQuardant == 4{
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
                }
                else if smallDistanceQuardant == 3 {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
                }
            }
            else if currentQuartdant == 1{
                if smallDistanceQuardant == 4 || smallDistanceQuardant == 3{
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
                }
                else if smallDistanceQuardant == 2 {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
                }
            }
            
            let directionNode = SCNNode()
            //            let translation = SCNMatrix4MakeTranslation(0, 0, Float(-100))
            //             //Rotate (yaw) around y axis
            //            let rotation = SCNMatrix4MakeRotation(-1 * smallDistanceAngle!, 0, 1, 0)
            //
            //
            //
            //             //Final transformation: TxR
            //            let transform = SCNMatrix4Mult(translation, rotation)
            //            arrowPlane?.transform = transform
            directionNode.name = "arrow"
            directionNode.geometry = plane
            //            directionNode.position = getCurrentcursorpos()!
            directionNode.position = SCNVector3Make(0, 0, -100.2)
            // let constraint = SCNLookAtConstraint(target: sceneView.pointOfView)
            
            // Keep the rotation on the horizon
            // constraint.isGimbalLockEnabled = true
            
            // Slow the constraint down a bit
            // constraint.influenceFactor = 0.01
            // directionNode.constraints = [constraint]
            sceneView.pointOfView?.addChildNode(directionNode)
            //            sceneNode?.addChildNode(directionNode)
            return
        }
        //let translation = SCNMatrix4MakeTranslation(0, 0, Float(-1))
        // Rotate (yaw) around y axis
        //        let rotation = SCNMatrix4MakeRotation(-1 * smallDistanceAngle!, 0, 0, 1)
        //
        //
        //
        //        // Final transformation: TxR
        //        //let transform = SCNMatrix4Mult(translation, rotation)
        //         arrowPlane?.transform = rotation
        //        let translationMatrix = MatrixHelper.translate(
        //            x: 0,
        //            y: 0,
        //            z: distance * -1
        //        )
        //        // Rotation matrix theta degrees
        //        let rotationMatrix = MatrixHelper.rotateAboutY(
        //            degrees: bearing * -1
        //        )
        // var transformMatrix = simd_mul(rotation, translation)
        
        //let plane = SCNPlane()
        //        let upperPoint =  (Int(smallDistanceAngle!) + 20 )
        //        let lowerPoint = (Int(smallDistanceAngle!) - 20)
        //        let range = lowerPoint...upperPoint
        //
        //        if (lowerPoint / 90) == 4 && (upperPoint/90) == 1 {
        //            if Int(direction) < lowerPoint {
        //                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
        //            }
        //            else   if Int(direction) > upperPoint {
        //                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
        //            }
        //            else{
        //                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "mystica-Arrow-set-Bulb-2-300px")
        //            }
        //        }
        //        else {
        //            var d1 = Int(direction) - lowerPoint
        //            var d2 = Int(direction) - upperPoint
        //            if range.contains(Int(direction)){
        //                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "mystica-Arrow-set-Bulb-2-300px")
        //            }
        //            else if Int(direction) < lowerPoint {
        //                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
        //            }
        //            else if Int(direction) > upperPoint {
        //                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
        //            }
        //        }
        //
        let smallDistanceQuardant  = Int(ceil(smallDistanceAngle! / 90.0))
        let currentQuartdant  = Int(ceil(direction / 90.0))
        var centerVal = smallDistanceAngle
        var lowerRange =  (Int(smallDistanceAngle! - 5))...Int(smallDistanceAngle!)
        var upperRange = (Int(smallDistanceAngle! + 1))...(Int(smallDistanceAngle! + 5))
        if (Int(smallDistanceAngle! - 5)) <= 360 && (Int(smallDistanceAngle! + 5)) >= 360 {
            // centerVal = 360
            lowerRange =  (Int(smallDistanceAngle! - 5))...360
            upperRange = (0)...(Int(smallDistanceAngle! + 5))
        }
        
        if lowerRange.contains(Int(direction)) || upperRange.contains(Int(direction)) {
            planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "mystica-Arrow-set-Bulb-2-300px")
        }
        else if currentQuartdant == 4 {
            if smallDistanceQuardant == 1 || smallDistanceQuardant == 2{
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
            }
            else if smallDistanceQuardant == 3 {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
            }
            else if Int(direction) < lowerRange.lowerBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
            }
            else if Int(direction) <  upperRange.upperBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
            }
        }
        else if currentQuartdant == 3 {
            if smallDistanceQuardant == 4 || smallDistanceQuardant == 1{
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
            }
            else if smallDistanceQuardant == 2 {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
            } else if Int(direction) < lowerRange.lowerBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
            }
            else if Int(direction) <  upperRange.upperBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
            }
        }
        else if currentQuartdant == 2{
            if smallDistanceQuardant == 1 || smallDistanceQuardant == 4{
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
            }
            else if smallDistanceQuardant == 3 {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
            }
            else if Int(direction) < lowerRange.lowerBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
            }
            else if Int(direction) <  upperRange.upperBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
            }
        }
        else if currentQuartdant == 1{
            if smallDistanceQuardant == 4 || smallDistanceQuardant == 3{
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
            }
            else if smallDistanceQuardant == 2 {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
            }
            else if Int(direction) < lowerRange.lowerBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-right-300px-1")
            }
            else if Int(direction) <  upperRange.upperBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "jean-victor-balin-arrow-go-left-300px")
            }
        }
        //let translation = SCNMatrix4MakeTranslation(0, 0, Float(-100))
        //Rotate (yaw) around y axis
        // let rotation = SCNMatrix4MakeRotation(-1 * smallDistanceAngle!, 0, 1, 0)
        
        
        
        //Final transformation: TxR
        //let transform = SCNMatrix4Mult(translation, rotation)
        // arrowPlane?.transform = transform
        //        let action = SCNAction.rotateBy(x: 0, y: CGFloat(GLKMathDegreesToRadians(90)), z: 0, duration: 0.5)
        //        arrowPlane.runAction(action)
        //
        //        //repositoning of the x,y,z axes after the rotation has been applied
        //        let currentPivot = boxNode.pivot
        //        let changePivot = SCNMatrix4Invert(boxNode.transform)
        //        boxNode.pivot = SCNMatrix4Mult(changePivot, currentPivot)
        //        boxNode.transform = SCNMatrix4Identity
        // arrowPlane?.position = getCurrentcursorpos()!
        
        //let constraint = SCNLookAtConstraint(target: sceneView.pointOfView)
        
        // Keep the rotation on the horizon
        //constraint.isGimbalLockEnabled = true
        
        // Slow the constraint down a bit
        //constraint.influenceFactor = 0.01
        //arrowPlane?.constraints = [constraint]
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    // MARK: - ARSCNViewDelegate
    
    public func renderer(_ renderer: SCNSceneRenderer, didRenderScene scene: SCNScene, atTime time: TimeInterval) {
        
        //updateLocation()
    }
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
        // updateLocation()
        return nil
    }
    
    public func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor){
        
    }
    public func renderer(_ renderer: SCNSceneRenderer, willUpdate node: SCNNode, for anchor: ARAnchor){
        
    }
    
    public func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor)
    {
        
    }
    public func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        
    }
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    func session(_: ARSession, didUpdate: ARFrame){
        
    }
    func session(_: ARSession, didAdd: [ARAnchor]){
        
    }
    func session(_: ARSession, didUpdate: [ARAnchor]) {
        
    }
    
    @IBAction func addLocations(_ sender: UIButton) {
        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) -> Void in
            node.removeFromParentNode()
        }
        LocationsList.removeAll()
        smallDistanceAngle = nil
        neaestDestination = nil
        isaddeed = true
        let add = ADDLocationVC(nibName: "ADDLocationVC", bundle: nil)
        self.navigationController?.pushViewController(add, animated: true)
    }
    
    @IBAction func clearNodes(_ sender: Any) {
        
        guard let node  = sceneNode else {
            return
        }
        sceneView.session.pause()
        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) -> Void in
            node.removeFromParentNode()
        }
        smallDistanceAngle = nil
        neaestDestination = nil
        isaddeed = true
        sceneView.session.run(sceneView.session.configuration!)
        let defaults = UserDefaults.standard
        //let placesData1 = NSKeyedArchiver.archivedData(withRootObject: [])
        defaults.set(nil, forKey: "Places")
        LocationsList.removeAll()
        UserDefaults.standard.synchronize()
        updateLocation()
    }
}
//extension ViewController : SCNSceneRendererDelegate{
//    public func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval){
//        //        updateLocation()
//
//        //        SCNTransaction.commit()
//    }
//    public func renderer(_ renderer: SCNSceneRenderer, didApplyAnimationsAtTime time: TimeInterval){
//
//    }
//    public func renderer(_ renderer: SCNSceneRenderer, didApplyConstraintsAtTime time: TimeInterval){
//
//    }
//
//
//}
//extension ViewController {
//
//}

